@echo off
echo Updating Haptic Software
echo Script version: 1.0



set git=%USERPROFILE%\Desktop\PortableGit\bin\git.exe

set root=%~dp0
cd /d %root%

%git% pull origin master



call desktopShortcut.bat Friction %root%hapstatics\ hapstatics.exe "%SystemRoot%\system32\SHELL32.dll, 249" friction
call desktopShortcut.bat Truss %root%hapstatics\ hapstatics.exe "%SystemRoot%\system32\SHELL32.dll, 159" truss
call desktopShortcut.bat Buoyancy %root%buoyancy\ Buoyancy.exe "%SystemRoot%\system32\SHELL32.dll, 246"
call desktopShortcut.bat ElectroMagnetism %root%EM\ 01-devices.exe "%SystemRoot%\system32\SHELL32.dll, 248"

call desktopShortcut.bat UpdateHapticSimulations %root% update.bat "%SystemRoot%\system32\SHELL32.dll, 238"


echo Update Complete, Press Enter to continue
pause