@echo off

set dest=%1
set srcPath=%2
set srcFile=%3
set iconStr=%4
set args=%5

set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"

echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%USERPROFILE%\Desktop\%dest%.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.WorkingDirectory = "%srcPath% " >> %SCRIPT%
echo oLink.TargetPath = "%srcPath%%srcFile%" >> %SCRIPT%
echo oLink.Arguments = "%args%" >> %SCRIPT% 
echo oLink.IconLocation = %iconStr% >> %SCRIPT%
echo oLink.Save >> %SCRIPT%

::echo %SCRIPT%
::type %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%